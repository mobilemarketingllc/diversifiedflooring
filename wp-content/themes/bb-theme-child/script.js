var $ = jQuery;
$('.product-detail-layout-6 .image-expander').on('click', function() {
		$('.img-responsive.toggle-image').trigger('click');	
});

$('.color_variations_slider_1 > .slides').slick({
    dots: false,
    infinite: false,
    speed: 300,
    arrows: true,
    slidesToShow: 4,
    slidesToScroll: 2,
    mobileFirst: false,
    prevArrow: '<a href="javascript:void(0)" class="arrow slick-prev">Previous</a>',
    nextArrow: '<a href="javascript:void(0)" class="arrow slick-next">Next</a>',
    responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 320,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
