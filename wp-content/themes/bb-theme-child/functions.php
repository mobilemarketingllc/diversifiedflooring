<?php
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
 //   wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
   
});

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


add_filter( 'wp_unique_post_slug', 'mg_unique_post_slug', 10, 6 ); 

function mg_unique_post_slug( $slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug ) {
 global $wpdb;
 
// don't change non-numeric values
 if ( ! is_numeric( $original_slug ) || $slug === $original_slug ) {
 return $slug;
 }
 
// Was there any conflict or was a suffix added due to the preg_match() call in wp_unique_post_slug() ?
 $post_name_check = $wpdb->get_var( $wpdb->prepare(
 "SELECT post_name FROM $wpdb->posts WHERE post_name = %s AND post_type IN ( %s, 'attachment' ) AND ID != %d AND post_parent = %d LIMIT 1",
 $original_slug, $post_type, $post_ID, $post_parent
 ) );
 
// There really is a conflict due to an existing page so keep the modified slug
 if ( $post_name_check ) {
 return $slug;
 }
 
// Return our numeric slug
 return $original_slug;
}

function bb_brand_menu_items() {
    ob_start();
    $vendor_page = get_page_by_path( '/vendor/', OBJECT, 'page' );
    ?> 
    <ul class="vendor_brand_menu_list">
    <?php
        wp_list_pages( array(
            'title_li'    => '',
            'child_of'    => $vendor_page->ID,
            'show_date'   => 'modified',
            'date_format' => $date_format
        ) );
    ?>
    </ul>
    <?php
    return ob_get_clean();
}
add_shortcode( 'BRAND_VENDOR_ITEMS', 'bb_brand_menu_items' );